//
//  WeatherApiProtocol.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 29/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
public protocol WeatherApiProtocol {
    
static func getCurrentWeather(getCurrentWeatherDto: GetCurrentWeatherDto, callback: @escaping (GetCurrentWeatherDto?, ErrorResponse?) -> Void)
}
