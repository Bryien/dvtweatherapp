//
//  ViewController.swift
//  DVTWeatherApp
//
//  Created by Bryien on 29/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import UIKit
import Foundation
import  SnapKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    fileprivate struct Item {

          var weatherItem : [GetCurrentWeatherDto] = []
    }
    
    private lazy var tableView = UITableView()
    
    
  
    private lazy var bottomSection = UIView()
   
    lazy var current: UILabel = UILabel()
    lazy var max: UILabel = UILabel()
    lazy var minValue: UILabel = UILabel()
    lazy var maxValue: UILabel = UILabel()
    lazy var currentValue: UILabel = UILabel()
    lazy var min: UILabel = UILabel()
    lazy var currentWeathervalue : UILabel = UILabel()
 
    
    
    
    public var refreshControl: UIRefreshControl?

    lazy var btn5 : UIButton = UIButton()
    let headerText = UILabel()
    var menuShown: Bool = false
    fileprivate var dataSource = Item()
    var dataone : String?
    

    
    
    
    // MARK: - View lifecycle
    
    override public func loadView() {
        super.loadView()
        
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        
       // UIView
        min.font = UIFont.boldSystemFont(ofSize: 12)
        min.textColor = UIColor.darkGray
        min.numberOfLines = 1
        min.text = ""
        min.textAlignment = .center
        view.addSubview(min)
        
        max.font = UIFont.boldSystemFont(ofSize: 12)
        max.textColor = UIColor.darkGray
        max.numberOfLines = 1
        max.textAlignment = .center
        max.text = ""
        view.addSubview(max)
        
        
        
        minValue.font = UIFont.boldSystemFont(ofSize: 10)
        minValue.textColor = UIColor.darkGray
        minValue.numberOfLines = 1
        minValue.textAlignment = .center
        minValue.text = "min"
        view.addSubview(minValue)
        
        
        maxValue.textColor = UIColor.black
        maxValue.font = UIFont.boldSystemFont(ofSize: 10.0)
        maxValue.textColor = UIColor.darkGray
        maxValue.numberOfLines = 1
        maxValue.text = "max"
        maxValue.textAlignment = .center
        view.addSubview(maxValue)
        
        
        currentValue.textColor = UIColor.darkGray
        currentValue.font = UIFont.boldSystemFont(ofSize: 10)
        currentValue.numberOfLines = 1
        currentValue.text = "Current"
        currentValue.textAlignment = .center
        view.addSubview(currentValue)
        
        current.textColor = UIColor.black
        current.font = UIFont.boldSystemFont(ofSize: 16.0)
        current.numberOfLines = 1
        current.textAlignment = .center
        view.addSubview(current)
        
        
        //table
        view.backgroundColor = UIColor.white
        headerText.backgroundColor = UIColor.white
        headerText.text = ""
        headerText.textAlignment = .center
        headerText.textColor = UIColor.orange
        headerText.font = UIFont.boldSystemFont(ofSize: 22)
        view.addSubview(headerText)
        
        tableView.backgroundColor =  UIColor.white
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        tableView.rowHeight = 80
        tableView.tableFooterView = UIView()
        tableView.register(DataDisplayTableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(tableView)
        
        makeConstraints()
        
        
    }
    
    
    func makeConstraints() {
        
        
        headerText.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(view).offset(230)
            make.centerX.equalTo(view)
        }
      minValue.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(headerText.snp.bottom).offset(26)
            make.left.equalTo(view.snp.left).offset(5)
            make.height.equalTo(25)
        
            
        }
        maxValue.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(headerText.snp.bottom).offset(26)
            make.right.equalTo(view.snp.right).offset(-5)
            make.height.equalTo(25)
            
            
        }
     
        currentValue.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(headerText.snp.bottom).offset(26)
            make.height.equalTo(25)
            make.centerX.equalTo(view)
            make.width.equalTo(view).dividedBy(2)
           
        }
        max.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(maxValue.snp.bottom).offset(15)
            make.right.equalTo(view.snp.right).offset(-8)
            make.height.equalTo(25)
            make.width.equalTo(view).dividedBy(2)
            
        }
        
        
        min.snp.makeConstraints { (make) in
            make.top.equalTo(minValue.snp.bottom).offset(16)
            make.width.equalTo(view.snp.width).dividedBy(8)
            make.height.equalTo(view.snp.width).dividedBy(8)
            
        }
        current.snp.makeConstraints { (make) in
            make.top.equalTo(currentValue.snp.bottom).offset(4)
            make.width.equalTo(view.snp.width).dividedBy(4)
            make.height.equalTo(20)
            
        }
       
        tableView.isHidden = false
        tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(minValue.snp.bottom).offset(16)
            make.left.equalTo(view).offset(12)
            make.right.equalTo(view).offset(-12)
            make.bottom.equalTo(view).offset(-12)
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = Item()
        dataSource.weatherItem = []

        
    }
    func getCurrentLocation() {
        WeatherApi.getCurrentWeather(getCurrentWeatherDto: GetCurrentWeatherDto.init()) { (response,error) in
            guard response != nil, let weatherData = response else {
                print("There was an error getting weather data")
                return
            }
            let baseOne = weatherData.base
            
            
        }
        return
    }

    
    private func createTableFooter() -> UIView {
        let margin: CGFloat = 20
        
        let label = UILabel()
        label.font = .systemFont(ofSize: 12)
        label.textColor =  UIColor.white
        label.text = NSLocalizedString("BALANCE_REFRESH", comment: "BALANCE_REFRESH")
        label.numberOfLines = 0
        let labelSize = label.sizeThatFits(CGSize(width: view.bounds.width - 2*margin, height: CGFloat.greatestFiniteMagnitude))
        //let labelSize = label.sizeThatFits(navigationController?.navigationBar.bounds.size ?? CGSize(width: 0, height: 44))
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: labelSize.height + 2*margin ))
        label.frame = UIEdgeInsetsInsetRect(footer.bounds, UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin))
        label.autoresizingMask = [.flexibleHeight, .flexibleHeight]
        label.textAlignment = .center
        footer.addSubview(label)
        
        footer.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(tableView.snp.bottom).offset(10)
        }
        
        return footer
    }
}
// MARK: - Data

extension ViewController {

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.weatherItem.count
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! DataDisplayTableViewCell
        
        
     //   let statementLine = dataSource.weatherItem[indexPath.row]
        
     
        cell.amountLabel.text =  dataone
        cell.ministatementDateLabel.text = dataone
            
        cell.transactionTypeLabel.text =  dataone
        
       
        
        return cell
    }
}






