//
//  NetworkingProtocol.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 29/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
public protocol NetworkingProtocol {
    
    static func call(url: String, callback: @escaping (ApiResponse?) -> Void)
    
}
