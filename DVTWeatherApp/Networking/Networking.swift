//
//  Networking.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 29/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//


import Foundation


public struct Networking<GetCurrentWeatherDto: Codable>: NetworkingProtocol  {
    
    
    /**This is a call to given url returning an object  GetCurrentWeatherDto
     - url: API Endpoint
     */
    public static func call(url: String, callback: @escaping (ApiResponse?) -> Void) {
        let requestUrl = url
        let urlComponents = URLComponents(string: requestUrl)
        let request = URLRequest(url: (urlComponents?.url)!)
        
    //Construct and send request
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) {(data, response, error) in
            DispatchQueue.main.async {
                if let err = error {
                    let errorResponse = Util.callbackErrorResponse(errorResponse: nil, errorCode: "091", errorDisplay: err.localizedDescription)
                    let apiResponse = ApiResponse(success: false, data: nil, error: errorResponse)
                    callback(apiResponse)
                }
                
                
                
                
                else if let jsonData = data{
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                         print(" creating current weather from JSON because: \(jsonData)")
                        let responseObject = try decoder.decode(GetCurrentWeatherDto.self, from: jsonData)
                         print(" creating current weather from JSON because: \(responseObject)")
                     
                        return callback(ApiResponse(success: true, data: responseObject, error: nil))
                        
                    } catch let err  {
                  
                  
                        let errorResponse = Util.callbackErrorResponse(errorResponse: nil, errorCode: "091", errorDisplay: err.localizedDescription)
                        let apiResponse = ApiResponse(success: false, data: nil, error: errorResponse)
                        callback(apiResponse)
                        
                        
                        print(err)
                        
                    }
                    
               
                }
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                else{
                    let errorResponse = Util.callbackErrorResponse(errorResponse: nil, errorCode: "091", errorDisplay: "An error occured")
                    let apiResponse = ApiResponse(success: false, data: nil, error: errorResponse)
                    callback(apiResponse)
                }
            }
        }
        task.resume()
    }
    
   
    
    
}
