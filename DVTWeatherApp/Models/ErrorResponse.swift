//
//  ErrorResponse.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 29/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
public struct ErrorResponse: Codable {
    public var error: String?
    public var errorDescription: String?
    public var errorDisplay: String?
    public var description: String?
    public var errorCode: String?
    
    public init() {
        
    }
}
