//
//  SysData.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 26/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation

public struct  SysData: Codable {
    public var type: Int?
    public var id: Int?
    public var message:  Double?
    public var country: String?
    public var sunrise: Int?
      public var sunset: Int?
    
    public init(){}
    

    
}

