//
//  WindData.swift
//  DVTWeatherApp
//
//  Created by Bryien on 30/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation

struct WindData: Codable {
    let speed: Double?
    let deg: Double?
}
