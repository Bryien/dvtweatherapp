//
//  GetCurrentWeatherDto.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 26/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
public struct GetCurrentWeatherDto : Codable {
    
     var coord: CoordinatesData?
     var weather:[WeatherInfoDto]?
     var base: String?
     var main: MainData?
     var visibility: Int?
     var wind: WindData?
     var clouds: CloudsData?
     var dt: Int?
     var sys: SysData?
     var timezone: Int?
     var id: Int?
     var name: String?
     var cod: Int?
    

    
    public init(){}

    
}

