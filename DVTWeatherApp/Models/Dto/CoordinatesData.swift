//
//  CoordinatesData.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 26/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
public struct  CoordinatesData: Codable {
    public var lon: Double?
    public var lat: Double?
    
  public init(){}

}
