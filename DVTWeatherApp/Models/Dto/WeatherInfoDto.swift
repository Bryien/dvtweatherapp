//
//  WeatherInfoDto.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 26/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
 public  struct WeatherInfoDto: Codable {
    let id: Int?
    let main: String?
    let description: String?
    let icon: String?
    
    
    
}



