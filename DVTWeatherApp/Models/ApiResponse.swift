//
//  ApiResponse.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 26/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
public struct ApiResponse {
    public let success: Bool
    public let data: Any?
    public let error: ErrorResponse?
}
