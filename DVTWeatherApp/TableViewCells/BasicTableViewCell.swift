//
//  BasicTableViewCell.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 27/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class BasicTableViewCell: UITableViewCell {
    
    lazy var titleLabel: UILabel = UILabel()
    convenience init() {
        self.init(style: .default, reuseIdentifier: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .clear
        accessoryView?.backgroundColor = UIColor.white
        selectionStyle = .none
        
        
        titleLabel.font = .systemFont(ofSize: 14)
        titleLabel.textColor = UIColor(named: "#0664ac")
        contentView.addSubview(titleLabel)
        
        setupLayoutConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Layout

extension BasicTableViewCell {
    private func setupLayoutConstraints() {
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(contentView.snp.leftMargin)
            make.top.equalTo(contentView.snp.topMargin)
            make.bottom.equalTo(contentView.snp.bottomMargin)
            make.right.equalTo(contentView.snp.rightMargin).priority(.low)
        }
    }
}
