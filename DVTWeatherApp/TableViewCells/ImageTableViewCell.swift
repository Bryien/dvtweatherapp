//
//  ImageTableViewCell.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 27/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
import UIKit


class ImageTableViewCell: BasicTableViewCell {
    
    lazy var iconImageView = UIImageView()
    
    convenience init() {
        self.init(style: .default, reuseIdentifier: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        
        titleLabel.backgroundColor = contentView.backgroundColor
        titleLabel.font = .systemFont(ofSize: 14)
        titleLabel.textColor = UIColor(named: "#073461")
        titleLabel.numberOfLines = 2
        contentView.addSubview(titleLabel)
        
        //iconImageView.backgroundColor = backgroundColor
        iconImageView.contentMode = .scaleAspectFit
        contentView.addSubview(iconImageView)
        
        setupLayoutConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Layout

extension ImageTableViewCell {
    
    struct Layout {
        static let imageSize = 45
        static let spacing = 10
    }
    
    private func setupLayoutConstraints() {
        iconImageView.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(contentView.snp.leftMargin)
            make.top.equalTo(contentView.snp.topMargin)
            make.bottom.equalTo(contentView.snp.bottomMargin)
            make.width.equalTo(Layout.imageSize)
            make.height.equalTo(Layout.imageSize)
        }
        
        titleLabel.snp.remakeConstraints { (make) -> Void in
            make.left.equalTo(iconImageView.snp.right).offset(Layout.spacing)
            make.top.equalTo(contentView.snp.topMargin)
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView.snp.rightMargin)
        }
    }
}
