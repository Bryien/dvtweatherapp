//
//  DataDisplayTableViewCell.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 8/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//
import Foundation
import UIKit
import SnapKit


final class DataDisplayTableViewCell : UITableViewCell {
    
    lazy var ministatementDateLabel: UILabel = UILabel()
    lazy var transactionTypeLabel: UILabel = UILabel()
    lazy var amountLabel: UILabel = UILabel()
    
    
    
    convenience init() {
        self.init(style: .default, reuseIdentifier: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .white
        contentView.backgroundColor = backgroundColor
        self.backgroundColor = backgroundColor
        selectionStyle = .none
        
        
        ministatementDateLabel.font = UIFont.boldSystemFont(ofSize: 12)
        ministatementDateLabel.textColor = UIColor.darkGray
        ministatementDateLabel.numberOfLines = 1
        ministatementDateLabel.text = "Date"
        ministatementDateLabel.textAlignment = .center
        contentView.addSubview(ministatementDateLabel)
        
        transactionTypeLabel.font = UIFont.boldSystemFont(ofSize: 12)
        transactionTypeLabel.textColor = UIColor.darkGray
        transactionTypeLabel.numberOfLines = 1
        //transactionTypeLabel.font = label.font.withSize(20)
        transactionTypeLabel.textColor  = UIColor(named: "#0664ac")
        transactionTypeLabel.textAlignment = .center
        transactionTypeLabel.text = ""
        
        contentView.addSubview(transactionTypeLabel)
        
        amountLabel.font = UIFont.boldSystemFont(ofSize: 10)
        amountLabel.textColor = UIColor(named: "#0664ac")
        amountLabel.numberOfLines = 1
        amountLabel.text = ""
        contentView.addSubview(amountLabel)
        
        
        
        
        
        setupLayoutConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Layout

extension DataDisplayTableViewCell {
    private struct Layout {
        static let vSpacing = 2
        static let spacing = 5
    }
    
    private func setupLayoutConstraints() {
        
        //MinistatementDate
        ministatementDateLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(contentView.snp.top).offset(5)
            make.left.equalTo(transactionTypeLabel.snp.left).offset(2)
            make.height.equalTo(25)
            make.width.equalTo(contentView)
            
        }
        ministatementDateLabel.textAlignment = .center
        
        
        
        //TransationType
        transactionTypeLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(contentView.snp.top).offset(5)
            make.left.equalTo(ministatementDateLabel.snp.left).offset(10)
            make.height.equalTo(25)
            make.width.equalTo(contentView).offset(-120)
            
        }
        transactionTypeLabel.textAlignment = .left
        amountLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(contentView.snp.top).offset(5)
            //make.right.equalTo(contentView.snp.right).offset(-5)
            make .left.equalTo(transactionTypeLabel.snp.right).offset(10)
            make.height.equalTo(25)
            make.width.equalTo(contentView).dividedBy(5)
        }
        amountLabel.textAlignment = .left
        
        
    }
    
    
    
}
