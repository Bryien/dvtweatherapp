//
//  BaseTableViewController.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 27/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation
import UIKit
open  class BaseTableViewController: UITableViewController {
    
    override init(style: UITableView.Style) {
        super.init(style: .grouped)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.estimatedRowHeight = 20
        tableView.rowHeight = 90
        tableView.backgroundColor = UIColor(named: "#ececec")
        // TODO: remove this when dropping iOS 8 support
        if #available(iOS 9, *) { }
        else { tableView.tableFooterView = tableView.tableFooterView ?? UIView() /* hide separators */ }
        
        
        
    }
}
