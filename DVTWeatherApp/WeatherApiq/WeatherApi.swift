//
//  WeatherApi.swift
//  WeatherMobileApplication
//
//  Created by Bryien on 29/8/2019.
//  Copyright © 2019 Bryien. All rights reserved.
//

import Foundation

public struct WeatherApi: WeatherApiProtocol {
    

    
    
    public static func getCurrentWeather(getCurrentWeatherDto: GetCurrentWeatherDto,callback: @escaping (GetCurrentWeatherDto?, ErrorResponse?) -> Void) {
        
       let baseUrl = Api.baseUrl
       let apiKey = Api.baseMapAPIKey
        let city = "London"
        
        
        let URLString = "\(baseUrl)?q=\(city)&APPID=\(apiKey)"
       
    Networking<GetCurrentWeatherDto>
            .call(url: URLString)
            { apiResponse in
                
                if (apiResponse?.success)! {
                    guard let data = apiResponse?.data as? GetCurrentWeatherDto else {
                        callback(nil, Util.callbackErrorResponse(errorResponse: apiResponse?.error, errorCode: "091", errorDisplay: "An error occurred"))
                        return
                    }
                    if !((data.weather != nil)) {
                        
   
                    }
                    callback(data, nil)
                } else {
                    callback(nil, Util.callbackErrorResponse(errorResponse: apiResponse?.error, errorCode: "091", errorDisplay: "An error occurred"))
                }
                
        }
    }
    
     func createWeatherObjectWith(json: Data, completion: @escaping (_ data: GetCurrentWeatherDto?, _ error: Error?) -> Void) {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let weather = try decoder.decode(GetCurrentWeatherDto.self, from: json)
            return completion(weather, nil)
        } catch let error {
            print("Error creating current weather from JSON because: \(error.localizedDescription)")
            return completion(nil, error)
        }
    }
    
    
    
    
    
    

}
    

